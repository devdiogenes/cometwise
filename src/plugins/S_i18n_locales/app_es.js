export default {
  APP_NAME: "DASHBOARD",

  APP_TEXTS_LINK_DUMMY: "",
  APP_TEXTS_CW_TXT_LEGAL: "Aviso legal",
  APP_TEXTS_CW_TXT_COOKIES: "Política de cookies",
  APP_TEXTS_CW_TXT_PRIVACY: "Política de privacidad",
  APP_TEXTS_CW_TXT_ECOMMERCE_TERMS: "Condiciones generales de contratación",
  APP_TEXTS_NOTIFICATION_MESSAGE: "Enlace copiado en el portapapeles",
  APP_SAVE_NOTIFICATION_MESSAGE: "Cambios guardados correctamente",
  APP_TEXTS_CW_LINK_USAGE:
    "Aquí puedes copiar los links a tus textos para utilizarlos donde lo necesites.",
  APP_TEXTS_CW_LEGAL_TITLE: "Textos legales",
  APP_TEXTS_CW_COOKIES_TITLE: "Instalación de tu widget de cookies",
  APP_TEXTS_CW_COOKIES_SUBTITLE:
    "Copia el siguiente código y pégalo justo después de la etiqueta",
  APP_TEXTS_CW_COOKIES_SCRIPT: "",
  APP_TEXTS_CW_COOKIES_HELP_TITLE:
    "Puedes ver más instrucciones sobre la instalación ",
  APP_TEXTS_CW_COOKIES_HELP_LINK: "aquí",
  APP_TEXTS_CW_COOKIES_NOTICE:
    "Este script se genera con la información sobre cookies que se haya introducido en el momento de suscribir el plan. Si en ese momento no has introducido ninguna información, no te preocupes, durante los próximos días se completará automaticamente. No tienes que hacer nada.",
  APP_TEXTS_CW_COPY_CODE_BUTTON: "copiar código",
  APP_TEXTS_HELP_TITLE: "¿Cómo instalo mis textos legales?",
  APP_TEXTS_HELP_DESCRIPTION_1:
    "Sólo tienes que copiar el enlace que aparece debajo de cadaapartado pulsando del botón 'copiar' que hay justo debajo. Pégalo en tu web en el lugar donde referencies ese apartado. Suelen estar en el menú inferior de la página (footer), con un apartado 'URL' para cada texto, ahí es donde debes pegar estos enlaces.",
  APP_TEXTS_HELP_TITLE_DESCRIPTION_2:
    "Si no estás familiarizado con este trabajo, pídele a tu desarrollador o administrador de la web que haga esto por ti.",

  APP_TEXTS_COOKIES_HELP_TITLE: "¿Cómo instalo mi widget de Cookies?",
  APP_TEXTS_COOKIES_HELP_DESCRIPTION_1:
    "Sólo tienes que copiar el código que aparece en el recuadro amarillo con el botón 'copiar código' y pégalo en tu sitio web, justo después de la etiqueta </body>. Es muy importante que coloques la línea de código que te damos inmediatamente después de la etiqueta </body>. Si no tienes mucha experiencia con el desarrollo web, un especialista o la persona que administra tu web podrá ayudarte en este paso.",
  APP_TEXTS_COOKIES_HELP_DESCRIPTION_2:
    "¡Con esto es suficiente para tener tu widget de cookies funcionando en tu web! Verás que aparece en el pie de página cuando entras en la web por primera vez. Si no logras verlo, puede que tengas que borrar las cookies de tu navegador o utilizar una pestaña de incógnito para ver el resultado.",
  /////////////////////////////////////////
  APP_MY_SITES_TITLE: "Mis webs",
  APP_MY_SITES_TABLE_TITLE: "Resumen de mis webs",
  APP_MY_SITES_TABLE_COL_ID: "ID",
  APP_MY_SITES_TABLE_COL_PLAN: "Plan",
  APP_MY_SITES_TABLE_COL_DOMAIN: "Dominio",
  APP_MY_SITES_TABLE_COL_EXPIRY: "Válido hasta",
  APP_MY_SITES_TABLE_COL_TEXT_LEGAL: "Aviso legal",
  APP_MY_SITES_TABLE_COL_TEXT_COOKIES: "Aviso de cookies",
  APP_MY_SITES_TABLE_COL_TEXT_CGC: "Cond. Generales de contratación",
  APP_MY_SITES_TABLE_COL_TEXT_PRIVACY: "Aviso de privacidad",
  APP_MY_SITES_TABLE_COL_EDIT: "Editar",
  APP_MY_SITES_TABLE_COL_UPGRADE: "Actualizar plan",
  APP_MY_SITES_TABLE_COL_INFORMATION: "Información",

  APP_MY_SITES_TABLE_PER_PAGE: "Webs por página",
  APP_MY_SITES_TABLE_PAGINATION_FROM: "Mostrando ",
  APP_MY_SITES_TABLE_PAGINATION_TO: "de",
  APP_MY_SITES_TABLE_PAGINATION_TOTAL: "De un total de",

  APP_MY_SITES_TABLE_INFORMATION: "Textos & Enlaces",
  /////////////////////////////////////////

  APP_DASHBOARD_SUMMARY_ADD_SITE_TITTLE: "Añadir sitio web",
  APP_DASHBOARD_SUMMARY_ADD_SITE_BODY: "Puedes añadir un nuevo sitio aquí",
  APP_DASHBOARD_SUMMARY_ADD_SITE_FOOTER: "",
  /////////////////////////////////////////
  APP_DASHBOARD_SUMMARY_MY_SITE_TITTLE: "Mis sitios web",
  APP_DASHBOARD_SUMMARY_MY_SITE_BODY:
    "Aquí puedes ver la información de tus sitios",
  APP_DASHBOARD_SUMMARY_MY_SITE_FOOTER: "",
  /////////////////////////////////////////
  APP_ADD_SITE_TYPE_TITLE: "¿Qué tipo de sitio es?",
  APP_ADD_SITE_TYPE_DESCRIPTION_WEB:
    "Blogs o sitios web informativos/corporativos donde no se realice comercio electrónico directo de artículos y/o productos o contratación electrónica de bienes y/o servicios de ningún tipo.",
  APP_ADD_SITE_TYPE_DESCRIPTION_ECOMMERCE:
    "Tiendas online de artículos y/o productos con almacén propio o dropshipping. ",
  APP_ADD_SITE_TYPE_DESCRIPTION_ECOMMERCE_WARNING:
    "No son válidos para marketplaces, tiendas online de afiliados Amazon, AliExpress, eBay, Alibaba o similares o plataformas de contratación electrónica de bienes y/o servicios. Para estos casos, solicita presupuesto personalizado sin compromiso en hola@cometwise.com  ",
  APP_ADD_SITE_BUTTON_LABEL: "Añadir nuevo sitio",
  APP_ADD_SITE_SITE_WEB: "Web",
  APP_ADD_SITE_SITE_WEB_ICON: "web",
  APP_ADD_SITE_SITE_ECOMMERCE: "Ecommerce",
  APP_ADD_SITE_SITE_ECOMMERCE_ICON: "shopping_cart",

  /////////////////////////////////////////
  APP_ADD_SITE_FORM_GENERAL_TITLE: "Información legal",
  APP_ADD_SITE_FORM_GENERAL_INTRO: "Sobre el titular del sitio web",
  APP_ADD_SITE_FORM_GENERAL_TITLE_PERSON: "Información de la persona jurídica",
  APP_ADD_SITE_FORM_GENERAL_SUBTITLE_PERSON: "  Datos Registrales BORME",
  APP_ADD_SITE_FORM_GENERAL_TITLE_ACTIVITY: "Información de la actividad",

  APP_ADD_SITE_FORM_GENERAL_REGULATED: "¿Es una actividad regulada?",
  APP_ADD_SITE_FORM_GENERAL_REGULATED_ENTITY: "Colegio profesional",
  APP_ADD_SITE_FORM_GENERAL_REGULATED_ID: "Número de colegiado",
  APP_ADD_SITE_FORM_GENERAL_REGULATED_TITLE: "Título académico",
  APP_ADD_SITE_FORM_GENERAL_REGULATED_COUNTRY: "País",
  APP_ADD_SITE_FORM_GENERAL_REGULATED_EQUIVALENT: "Equivalente",
  APP_ADD_SITE_FORM_GENERAL_REGULATED_APPLICABLE: "Regulación que se aplica",
  APP_ADD_SITE_FORM_GENERAL_REGULATED_SOURCES: "Información",

  APP_ADD_SITE_FORM_GENERAL_LEGAL_ENTITY: "¿Es una persona jurídica?",
  APP_ADD_SITE_FORM_GENERAL_LEGAL_ENTITY_CITY: "Registro Mercantil de (ciudad)",
  APP_ADD_SITE_FORM_GENERAL_LEGAL_ENTITY_TOMO: "Tomo",
  APP_ADD_SITE_FORM_GENERAL_LEGAL_ENTITY_BOOK: "Libro",
  APP_ADD_SITE_FORM_GENERAL_LEGAL_ENTITY_PAGE: "Folio",
  APP_ADD_SITE_FORM_GENERAL_LEGAL_ENTITY_SECTION: "Sección",
  APP_ADD_SITE_FORM_GENERAL_LEGAL_ENTITY_PARAGRAPH: "Hoja",
  APP_ADD_SITE_FORM_GENERAL_LEGAL_ENTITY_INSCRIPTION: "Inscripción",

  APP_ADD_SITE_FORM_GENERAL_DOMAIN: "Sitio web: https://... ",
  APP_ADD_SITE_FORM_GENERAL_SECTION_OWNER: "Sobre el titular del sitio...",
  APP_ADD_SITE_FORM_GENERAL_NAME: "Nombre y apellidos/razón social ",
  APP_ADD_SITE_FORM_GENERAL_ID: "NIF/NIE",
  APP_ADD_SITE_FORM_GENERAL_SECTION_ADDRESS: "Dirección",
  APP_ADD_SITE_FORM_GENERAL_ADDRESS: "Calle",
  APP_ADD_SITE_FORM_GENERAL_ADDRESS_POSTAL_CODE: "Código Postal",
  APP_ADD_SITE_FORM_GENERAL_ADDRESS_CITY: "Ciudad",
  APP_ADD_SITE_FORM_GENERAL_ADDRESS_COUNTY: "Provincia",
  APP_ADD_SITE_FORM_GENERAL_ADDRESS_COUNTRY: "País",
  APP_ADD_SITE_FORM_GENERAL_HOSTING: "Proveedor de hosting",
  APP_ADD_SITE_FORM_GENERAL_SECTION_CONTACT: "Datos de contacto",
  APP_ADD_SITE_FORM_GENERAL_EMAILS_BILLS:
    "Email para solicitar factura en papel",
  APP_ADD_SITE_FORM_GENERAL_PHONES: "Teléfono de contacto",
  APP_ADD_SITE_FORM_GENERAL_EMAILS: "Correo electrónico de contacto",
  APP_ADD_SITE_FORM_GENERAL_SECTION_SN: "Redes sociales",
  APP_ADD_SITE_FORM_GENERAL_SOCIAL_FACEBOOK: "Facebook",
  APP_ADD_SITE_FORM_GENERAL_SOCIAL_INSTAGRAM: "Instagram",
  APP_ADD_SITE_FORM_GENERAL_SOCIAL_YOUTUBE: "Youtube",
  APP_ADD_SITE_FORM_GENERAL_SOCIAL_TWITTER: "Twitter",
  APP_ADD_SITE_FORM_GENERAL_SOCIAL_LINKEDIN: "Linkedin",
  APP_ADD_SITE_FORM_GENERAL_SECTION_MISC: "Sobre nosotros",
  APP_ADD_SITE_FORM_GENERAL_ABOUT:
    "¿Alguna cosa sobre tí que nos quieras contar? =)",

  /////////////////////////////////////////
  APP_ADD_SITE_FORM_CONTACT_TITLE: "Recogida de información",
  APP_ADD_SITE_FORM_CONTACT_INTRO:
    "¿Que formularios electrónicos utilizas para la recogida de datos personales?",
  APP_ADD_SITE_FORM_CONTACT_ISLOGIN: "Login - Acceso",
  APP_ADD_SITE_FORM_CONTACT_ISLIVECHAT: "Chat vivo",
  APP_ADD_SITE_FORM_CONTACT_ISCONTACT: "Contacto",
  APP_ADD_SITE_FORM_CONTACT_ISNEWSLETTER: "Suscripción a newsletter",
  APP_ADD_SITE_FORM_CONTACT_ISREGISTER: "Registro - Crear cuenta",
  APP_ADD_SITE_FORM_CONTACT_ISBLOGCOMMENT: "Blog - Deja un comentario",
  APP_ADD_SITE_FORM_CONTACT_ISBUYGUEST: "Comprar como invitado",
  APP_ADD_SITE_FORM_CONTACT_SOLICITAR_PRESUPUESTO: "Solicitar presupuesto",

  /////////////////////////////////////////
  APP_ADD_SITE_FORM_COOKIES_TITLE: "Cookies",
  APP_ADD_SITE_FORM_COOKIES_NAME: "Nombre de la cookie",
  APP_ADD_SITE_FORM_COOKIES_INTRO: "¿Que cookies utiliza tu sitio web?",
  APP_ADD_SITE_FORM_COOKIE_NAME: "Nombre de la cookie",
  APP_ADD_SITE_FORM_COOKIE_PURPOSE: "Propósito de la cookie",
  APP_ADD_SITE_FORM_COOKIE_NATURE: "Naturaleza de la cookie",
  APP_ADD_SITE_FORM_COOKIES_PROVIDER: "Proveedor de la cookie",
  APP_ADD_SITE_FORM_COOKIES_EXPIRY: "Expiración de la cookie",
  APP_ADD_SITE_FORM_COOKIES_TYPE: "Tipo de Cookie",
  APP_ADD_SITE_FORM_COOKIES_EMPTY_INFO:
    "Rellena todos los campos para poder guardar",
  APP_ADD_SITE_FORM_COOKIES_BUTTON_ADD: "Añadir",
  APP_ADD_SITE_FORM_COOKIES_BUTTON_RESET: "Reset",
  APP_ADD_SITE_FORM_COOKIES_BUTTON_GENERATECOOKIE: "Generar Cookie",
  APP_ADD_SITE_FORM_COOKIES_NATURE_NECESSARY: "Necesarias",
  APP_ADD_SITE_FORM_COOKIES_NATURE_ANALYTICS: "Analíticas",
  /////////////////////////////////////////
  APP_ADD_SITE_FORM_PAYMENTS_TITLE: "Métodos de pago",
  APP_ADD_SITE_FORM_PAYMENTS_INTRO:
    "Seleccionar con un X todas las formas de pago admitidas para la realización de pedidos:",
  APP_ADD_SITE_FORM_PAYMENTS_CARD: "Tarjeta de crédito o débito",
  APP_ADD_SITE_FORM_PAYMENTS_BANK_TRANSFER: "Transferencia bancaria",
  APP_ADD_SITE_FORM_PAYMENTS_PAYPAL: "Paypal",
  APP_ADD_SITE_FORM_PAYMENTS_STRIPE: "Stripe",
  APP_ADD_SITE_FORM_PAYMENTS_BIZUM: "Bizum",
  APP_ADD_SITE_FORM_PAYMENTS_ON_DELIVERY: "Contrar reembolso",
  APP_ADD_SITE_FORM_PAYMENTS_PAYMENT_NAME: "Nombre del método de pago",
  APP_ADD_SITE_FORM_PAYMENTS_PAYMENT_DESRIPTION: "Descripción",
  APP_ADD_SITE_FORM_PAYMENTS_SECTION_ADD_TITLE:
    "¿No encuentras todos tus métodos de pago?",
  APP_ADD_SITE_FORM_PAYMENTS_SECTION_ADD_INTRO: "Añádelos aquí.",
  APP_ADD_SITE_FORM_PAYMENTS_PAYMENT_BUTTON_ADD: "Añadir",
  APP_ADD_SITE_FORM_PAYMENTS_PAYMENT_BUTTON_RESET: "Reset",
  APP_ADD_SITE_FORM_PAYMENTS_EXEMPT:
    "Este apartado sólo es necesario para webs. =)",

  /////////////////////////////////////////
  APP_ADD_SITE_FORM_ORDERS_TITLE: "Gestión de los pedidos",

  APP_ADD_SITE_FORM_ITEM_PROTECTION_TITLE: "Protección de los artículos",
  APP_ADD_SITE_FORM_ITEM_PROTECTION_INTRO:
    "¿Cómo proteges los artículos que envías?",

  APP_ADD_SITE_FORM_ITEM_PROTECTION_CARDBOARD: "Caja de cartón",
  APP_ADD_SITE_FORM_ITEM_PROTECTION_AIRBAGS: "Bolsas de aire",
  APP_ADD_SITE_FORM_ITEM_PROTECTION_BUBLE_PAPER: "Papel de burbujas",
  APP_ADD_SITE_FORM_ITEM_PROTECTION_INTERNAL: "Protección interna",
  APP_ADD_SITE_FORM_ITEM_PROTECTION_PLASTIC_COVER: "Estuche de plástico",
  APP_ADD_SITE_FORM_ITEM_PROTECTION_EXTERNAL_BOX: "Caja externa",
  APP_ADD_SITE_FORM_ITEM_PROTECTION_OTHERS: "Otros",

  APP_ADD_SITE_FORM_DELIVERIES_TITLE: "Entregas, empresas y costes",
  APP_ADD_SITE_FORM_DELIVERIES_INTRO:
    "Cuéntanos sobre tus envíos: ¿A donde lo mandas?¿Que costes tienes? ¿Cuanto tardan?¿Quien los envía?",
  APP_ADD_SITE_FORM_DELIVERIES_PREDEFINED_DELIVERY_GENERIC_COUNTRY:
    "Pais de destino",
  APP_ADD_SITE_FORM_DELIVERIES_PREDEFINED_DELIVERY_DELAY: "Ej: 48 horas",
  APP_ADD_SITE_FORM_DELIVERIES_PREDEFINED_DELIVERY_COST: "Ej: 5€",
  APP_ADD_SITE_FORM_DELIVERIES_PREDEFINED_DELIVERY_COMPANY: "Transportes ACME.",

  APP_ADD_SITE_FORM_DELIVERIES_PREDEFINED_DEST_PENINSULA:
    "Territorio peninsular Español",
  APP_ADD_SITE_FORM_DELIVERIES_PREDEFINED_DEST_BALEARES: "Islas Baleares",
  APP_ADD_SITE_FORM_DELIVERIES_PREDEFINED_DEST_CEUTA: "Ceuta",
  APP_ADD_SITE_FORM_DELIVERIES_PREDEFINED_DEST_MELILLA: "Melilla",
  APP_ADD_SITE_FORM_DELIVERIES_PREDEFINED_DEST_CANARIAS: "Islas Canarias",
  APP_ADD_SITE_FORM_DELIVERIES_PREDEFINED_DEST_EU: "Union Europea",
  APP_ADD_SITE_FORM_DELIVERIES_PREDEFINED_DEST_OTHERS: "Otros",

  APP_ADD_SITE_FORM_TRANSPORT_SUBTITLE: "Empresa de transporte",
  APP_ADD_SITE_FORM_TRANSPORT_NAME: "Nombre",
  APP_ADD_SITE_FORM_TRANSPORT_WEBSITE: "Web",
  APP_ADD_SITE_FORM_TRANSPORT_DELAY: "Tiempo",

  APP_ADD_SITE_FORM_LOCATIONS_SUBTITLE: "Entregas",
  APP_ADD_SITE_FORM_LOCATIONS_COUNTRY: "País",
  APP_ADD_SITE_FORM_LOCATIONS_COST: "Coste",
  APP_ADD_SITE_FORM_LOCATIONS_DELAY: "Tiempo",

  APP_ADD_SITE_FORM_LOCATIONS_WEIGHT: "Peso máximo (kg)",

  APP_ADD_SITE_FORM_CLICK_COLLECT_SUBTITLE:
    "¿Ofreces servicio 'click & collect'?",
  APP_ADD_SITE_FORM_CLICK_COLLECT_MON: "Lun",
  APP_ADD_SITE_FORM_CLICK_COLLECT_TUE: "Mar",
  APP_ADD_SITE_FORM_CLICK_COLLECT_WED: "Mie",
  APP_ADD_SITE_FORM_CLICK_COLLECT_THU: "Jue",
  APP_ADD_SITE_FORM_CLICK_COLLECT_FRI: "Vie",
  APP_ADD_SITE_FORM_CLICK_COLLECT_SAT: "Sab",
  APP_ADD_SITE_FORM_CLICK_COLLECT_SUM: "Dom",

  APP_ADD_SITE_FORM_PAYMENTS_DELIVERIES_BUTTON_ADD: "Añadir",
  APP_ADD_SITE_FORM_PAYMENTS_DELIVERIES_BUTTON_RESET: "Reset",

  /////////////////////////////////////////
  APP_FORM_EMPTY: "Por favor, introduce información en el formulario",
  APP_FORM_SUBMIT: "Guardar cambios",

  ////////////////////////////////////////

  APP_PLANS_TRANSFER_TEXT: `<p> Mandanos un email con el justificante y el sitio web que quieres actualizar a: </p>
                <p><a href="mailto:hola@cometwise.com">hola@cometwise.com</a></p>
                <p>IBAN: BE81 9670 1142 2724,</p>
                <p>*Esta opción puede tardar 2 o 3 días.</p>
                <p>Puedes comprobar todos nuestros datos <a href="./static/account_ownership_proof_eur.pdf" target="_blank">aquí</a></p>`
};

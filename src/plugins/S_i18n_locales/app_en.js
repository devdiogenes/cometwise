export default {
  APP_NAME: "DASHBOARD",

  APP_TEXTS_LINK_DUMMY: "",
  APP_TEXTS_CW_TXT_LEGAL: "Legal notice",
  APP_TEXTS_CW_TXT_COOKIES: "Cookies policy",
  APP_TEXTS_CW_TXT_PRIVACY: "Privacy policy",
  APP_TEXTS_CW_TXT_ECOMMERCE_TERMS: "Terms and Conditions",
  APP_TEXTS_NOTIFICATION_MESSAGE: "Enlace copiado en el portapapeles",
  APP_SAVE_NOTIFICATION_MESSAGE: "Cambios guardados correctamente",
  APP_TEXTS_CW_LINK_USAGE:
    "Aquí puedes copiar los links a tus textos para utilizarlos donde lo necesites.",
  APP_TEXTS_CW_LEGAL_TITLE: "Textos legales",
  APP_TEXTS_CW_COOKIES_TITLE: "Instalación de tu widget de cookies",
  APP_TEXTS_CW_COOKIES_SUBTITLE:
    "Copia el siguiente código y pégalo justo después de la etiqueta",
  APP_TEXTS_CW_COOKIES_SCRIPT: "",
  APP_TEXTS_CW_COOKIES_HELP_TITLE:
    "Puedes ver más instrucciones sobre la instalación ",
  APP_TEXTS_CW_COOKIES_HELP_LINK: "aquí",
  APP_TEXTS_CW_COOKIES_NOTICE:
    "Este script se genera con la información sobre cookies que se haya introducido en el momento de suscribir el plan. Si en ese momento no has introducido ninguna información, no te preocupes, durante los próximos días se completará automaticamente. No tienes que hacer nada.",
  APP_TEXTS_CW_COPY_CODE_BUTTON: "copy code",
  APP_TEXTS_HELP_TITLE: "¿Cómo instalo mis textos legales?",
  APP_TEXTS_HELP_DESCRIPTION_1:
    "Sólo tienes que copiar el enlace que aparece debajo de cadaapartado pulsando del botón 'copiar' que hay justo debajo. Pégalo en tu web en el lugar donde referencies ese apartado. Suelen estar en el menú inferior de la página (footer), con un apartado 'URL' para cada texto, ahí es donde debes pegar estos enlaces.",
  APP_TEXTS_HELP_TITLE_DESCRIPTION_2:
    "Si no estás familiarizado con este trabajo, pídele a tu desarrollador o administrador de la web que haga esto por ti.",

  APP_TEXTS_COOKIES_HELP_TITLE: "¿Cómo instalo mi widget de Cookies?",
  APP_TEXTS_COOKIES_HELP_DESCRIPTION_1:
    "Sólo tienes que copiar el código que aparece en el recuadro amarillo con el botón 'copiar código' y pégalo en tu sitio web, justo después de la etiqueta </body>. Es muy importante que coloques la línea de código que te damos inmediatamente después de la etiqueta </body>. Si no tienes mucha experiencia con el desarrollo web, un especialista o la persona que administra tu web podrá ayudarte en este paso.",
  APP_TEXTS_COOKIES_HELP_DESCRIPTION_2:
    "¡Con esto es suficiente para tener tu widget de cookies funcionando en tu web! Verás que aparece en el pie de página cuando entras en la web por primera vez. Si no logras verlo, puede que tengas que borrar las cookies de tu navegador o utilizar una pestaña de incógnito para ver el resultado.",
  /////////////////////////////////////////
  APP_MY_SITES_TITLE: "My sites",
  APP_MY_SITES_TABLE_TITLE: "My sites summary",
  APP_MY_SITES_TABLE_COL_ID: "ID",
  APP_MY_SITES_TABLE_COL_PLAN: "Plan",
  APP_MY_SITES_TABLE_COL_DOMAIN: "Domain",
  APP_MY_SITES_TABLE_COL_EXPIRY: "Expiry date",
  APP_MY_SITES_TABLE_COL_TEXT_LEGAL: "Legal notice",
  APP_MY_SITES_TABLE_COL_TEXT_COOKIES: "Cookies notice",
  APP_MY_SITES_TABLE_COL_TEXT_CGC: "Terms",
  APP_MY_SITES_TABLE_COL_TEXT_PRIVACY: "Privacy",
  APP_MY_SITES_TABLE_COL_EDIT: "Edit",
  APP_MY_SITES_TABLE_COL_UPGRADE: "Upgrade plan",
  APP_MY_SITES_TABLE_COL_INFORMATION: "Information",

  APP_MY_SITES_TABLE_PER_PAGE: "Sites per page",
  APP_MY_SITES_TABLE_PAGINATION_FROM: "Showing ",
  APP_MY_SITES_TABLE_PAGINATION_TO: "to",
  APP_MY_SITES_TABLE_PAGINATION_TOTAL: "from total of",

  APP_MY_SITES_TABLE_INFORMATION: "My texts and links",
  /////////////////////////////////////////
  APP_ADD_SITE_BUTTON_LABEL: "Add new site",
  APP_ADD_SITE_TYPE_TITLE: "What type of website is?",
  APP_ADD_SITE_TYPE_DESCRIPTION_WEB: "Lorem ipsu lorem ipsu lorem ipsu",
  APP_ADD_SITE_TYPE_DESCRIPTION_ECOMMERCE:
    "Lorem ipsu lorem ipsu lorem ipsu eccomerce",

  APP_ADD_SITE_SITE_WEB: "Web",
  APP_ADD_SITE_SITE_WEB_ICON: "web",
  APP_ADD_SITE_SITE_ECOMMERCE: "Ecommerce",
  APP_ADD_SITE_SITE_ECOMMERCE_ICON: "shopping_cart",
  /////////////////////////////////////////
  APP_ADD_SITE_FORM_GENERAL_TITLE: "Legal info",
  APP_ADD_SITE_FORM_GENERAL_INTRO: "",

  APP_ADD_SITE_FORM_GENERAL_REGULATED: "Is it a regulated activity?",
  APP_ADD_SITE_FORM_GENERAL_REGULATED_ENTITY: "Entity",
  APP_ADD_SITE_FORM_GENERAL_REGULATED_ID: "Registered ID",
  APP_ADD_SITE_FORM_GENERAL_REGULATED_TITLE: "Title",
  APP_ADD_SITE_FORM_GENERAL_REGULATED_COUNTRY: "Country",
  APP_ADD_SITE_FORM_GENERAL_REGULATED_EQUIVALENT: "Equivalent",
  APP_ADD_SITE_FORM_GENERAL_REGULATED_APPLICABLE: "Applicable regulations",
  APP_ADD_SITE_FORM_GENERAL_REGULATED_SOURCES: "Information source",

  APP_ADD_SITE_FORM_GENERAL_LEGAL_ENTITY: "Is it a legal entity?",
  APP_ADD_SITE_FORM_GENERAL_LEGAL_ENTITY_CITY: "City",
  APP_ADD_SITE_FORM_GENERAL_LEGAL_ENTITY_BOOK: "Book",
  APP_ADD_SITE_FORM_GENERAL_LEGAL_ENTITY_PAGE: "Page",
  APP_ADD_SITE_FORM_GENERAL_LEGAL_ENTITY_SECTION: "Section",
  APP_ADD_SITE_FORM_GENERAL_LEGAL_ENTITY_PARAGRAPH: "Paragraph",
  APP_ADD_SITE_FORM_GENERAL_LEGAL_ENTITY_INSCRIPTION: "Inscription",

  APP_ADD_SITE_FORM_GENERAL_DOMAIN: "Domain",
  APP_ADD_SITE_FORM_GENERAL_NAME: "Registered Name",
  APP_ADD_SITE_FORM_GENERAL_ID: "Company ID",
  APP_ADD_SITE_FORM_GENERAL_ADDRESS: "Registered Address",
  APP_ADD_SITE_FORM_GENERAL_HOSTING: "Hosting provider",
  APP_ADD_SITE_FORM_GENERAL_PHONES: "Contact phone",
  APP_ADD_SITE_FORM_GENERAL_EMAILS: "Contact email",
  APP_ADD_SITE_FORM_GENERAL_ABOUT: "About ",
  APP_ADD_SITE_FORM_GENERAL_SOCIAL_FACEBOOK: "Facebook",
  APP_ADD_SITE_FORM_GENERAL_SOCIAL_INSTAGRAM: "Instagram",
  APP_ADD_SITE_FORM_GENERAL_SOCIAL_YOUTUBE: "Youtube",
  APP_ADD_SITE_FORM_GENERAL_SOCIAL_TWITTER: "Twitter",
  APP_ADD_SITE_FORM_GENERAL_SOCIAL_LINKEDIN: "Linkedin",
  APP_ADD_SITE_FORM_GENERAL_EMAILS_BILLS: "Email to get paper bills",

  /////////////////////////////////////////
  APP_ADD_SITE_FORM_CONTACT_TITLE: "Contact info",
  APP_ADD_SITE_FORM_CONTACT_INTRO:
    "What kind of contact forms do you offer beauty?",
  APP_ADD_SITE_FORM_CONTACT_ISLOGIN: "Login",
  APP_ADD_SITE_FORM_CONTACT_ISLIVECHAT: "Live chat",
  APP_ADD_SITE_FORM_CONTACT_ISCONTACT: "Contact",
  APP_ADD_SITE_FORM_CONTACT_ISNEWSLETTER: "Newsletter suscription",
  APP_ADD_SITE_FORM_CONTACT_ISREGISTER: "Register",
  APP_ADD_SITE_FORM_CONTACT_ISBLOGCOMMENT: "Comments",
  APP_ADD_SITE_FORM_CONTACT_ISBUYGUEST: "Buy as a guest",
  APP_ADD_SITE_FORM_CONTACT_SOLICITAR_PRESUPUESTO: "Request quotation",

  /////////////////////////////////////////
  APP_ADD_SITE_FORM_COOKIES_TITLE: "Cookies",
  APP_ADD_SITE_FORM_COOKIES_INTRO: "What cookies do you use?",
  APP_ADD_SITE_FORM_COOKIES_NAME: "Cookie name",
  APP_ADD_SITE_FORM_COOKIES_PURPOSE: "Cookie purpose",
  APP_ADD_SITE_FORM_COOKIES_EMPTY_INFO: "Please fill the info before save",
  APP_ADD_SITE_FORM_COOKIES_BUTTON_ADD: "Add",
  APP_ADD_SITE_FORM_COOKIES_BUTTON_RESET: "Reset",
  APP_ADD_SITE_FORM_COOKIES_BUTTON_GENERATECOOKIE: "Generate Cookie",
  APP_ADD_SITE_FORM_COOKIES_NATURE_NECESSARY: "Necessary",
  APP_ADD_SITE_FORM_COOKIES_NATURE_ANALYTICS: "Analytics",

  /////////////////////////////////////////
  APP_ADD_SITE_FORM_PAYMENTS_TITLE: "Payment methods",
  APP_ADD_SITE_FORM_PAYMENTS_INTRO: "Payment methods",
  APP_ADD_SITE_FORM_PAYMENTS_CARD: "Credit/Debit card",
  APP_ADD_SITE_FORM_PAYMENTS_BANK_TRANSFER: "Bank transfer",
  APP_ADD_SITE_FORM_PAYMENTS_PAYPAL: "Paypal",
  APP_ADD_SITE_FORM_PAYMENTS_STRIPE: "Stripe",
  APP_ADD_SITE_FORM_PAYMENTS_BIZUM: "Bizum",
  APP_ADD_SITE_FORM_PAYMENTS_ON_DELIVERY: "",
  APP_ADD_SITE_FORM_PAYMENTS_PAYMENT_NAME: "Payment name",
  APP_ADD_SITE_FORM_PAYMENTS_PAYMENT_DESRIPTION: "Payment description",
  APP_ADD_SITE_FORM_PAYMENTS_PAYMENT_BUTTON_ADD: "Add",
  APP_ADD_SITE_FORM_PAYMENTS_PAYMENT_BUTTON_RESET: "Reset",

  /////////////////////////////////////////
  APP_ADD_SITE_FORM_DELIVERIES_TITLE: "Transport & Deliveries",
  APP_ADD_SITE_FORM_DELIVERIES_INTRO: "",

  APP_ADD_SITE_FORM_TRANSPORT_SUBTITLE: "Transport companies",
  APP_ADD_SITE_FORM_TRANSPORT_NAME: "Name",
  APP_ADD_SITE_FORM_TRANSPORT_WEBSITE: "Web",
  APP_ADD_SITE_FORM_TRANSPORT_DELAY: "Delay",

  APP_ADD_SITE_FORM_LOCATIONS_SUBTITLE: "Deliveries",
  APP_ADD_SITE_FORM_LOCATIONS_COUNTRY: "Country",
  APP_ADD_SITE_FORM_LOCATIONS_COST: "Cost",
  APP_ADD_SITE_FORM_LOCATIONS_DELAY: "Delay",

  APP_ADD_SITE_FORM_LOCATIONS_WEIGHT: "Max. weight (kg)",

  APP_ADD_SITE_FORM_CLICK_COLLECT_SUBTITLE:
    "Do you offer click and collect service?",
  APP_ADD_SITE_FORM_CLICK_COLLECT_MON: "Mon",
  APP_ADD_SITE_FORM_CLICK_COLLECT_TUE: "Tue",
  APP_ADD_SITE_FORM_CLICK_COLLECT_WED: "Wed",
  APP_ADD_SITE_FORM_CLICK_COLLECT_THU: "Thu",
  APP_ADD_SITE_FORM_CLICK_COLLECT_FRI: "Fri",
  APP_ADD_SITE_FORM_CLICK_COLLECT_SAT: "Sat",
  APP_ADD_SITE_FORM_CLICK_COLLECT_SUM: "Sun",

  APP_ADD_SITE_FORM_PAYMENTS_DELIVERIES_BUTTON_ADD: "Add",
  APP_ADD_SITE_FORM_PAYMENTS_DELIVERIES_BUTTON_RESET: "Reset",

  /////////////////////////////////////////
  APP_FORM_EMPTY: "Please fill out all information",
  APP_FORM_SUBMIT: "Save changes",

  ////////////////////////////////////////

  APP_PLANS_TRANSFER_TEXT: `<p> Mandanos un email con el justificante y el sitio web que quieres actualizar a: </p>
                <p><a href="mailto:hola@cometwise.com">hola@cometwise.com</a></p>
                <p>IBAN: BE81 9670 1142 2724,</p>
                <p>*Esta opción puede tardar 2 o 3 días.</p>
                <p>Puedes comprobar todos nuestros datos <a href="./static/account_ownership_proof_eur.pdf" target="_blank">aquí</a></p>`
};

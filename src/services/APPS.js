import BF_APP_COMMON from "./BF_APP_COMMON";
export default class APPS extends BF_APP_COMMON {
  constructor(router) {
    super(router);
  }
  //////////////////////////////////////////////////////////
  ///   COOKIES
  async cookies_get_all() {
    /**
     * Entradas: id_domain
        Salidas:  un array de objetos Cookie asociados al dominio
    */
    return await this._call_service("/CookieGetAll");
  }
  async cookies_insert() {
    /**
     * Entradas: name, purpose, id_domain.
      "payload": {
          "name": "Alcachofa",
          "purpose": "Adueñarme del mundo",
          "id_domain": "1"
        }
    */
    return await this._call_service("/CookieInsert");
  }
  async cookies_update() {
    /**
     * Entradas: id_cookie, name, purpose, id_domain.
        "payload": {
            "name": "Alcachofa",
            "purpose": "Adueñarme del mundo",
            "id_domain": "1"
            }
      */
    return await this._call_service("/CookieUpdate");
  }
  async cookies_delete() {
    /**
     * Entradas: id_cookie.
      "payload": "Cookie deleted."
     * */
    return await this._call_service("/CookieDelete");
  }
  //////////////////////////////////////////////////////////
  ///   DOMAINS
  async domain_get_all() {
    return await this._call_service("/DomainGetAll")
      .then(response => {
        return response.data.payload;
      })
      .catch(() => {});
  }

  async domain_get_sample() {
    return await this._call_service("/DomainGetSample");
  }

  async domain_form() {
    if (localStorage.getItem("op_id_rup") === null) {
      return await this.#domain_insert();
    } else {
      return await this.#domain_update();
    }
  }
  async #domain_insert() {
    return await this._call_service("/DomainInsert")
      .then(response => {
        localStorage.setItem("op_id_rup", response.data.payload.id_rup);
      })
      .catch(() => {});
  }
  async #domain_update() {
    this.data["id_rup"] = localStorage.getItem("op_id_rup");
    return await this._call_service("/DomainUpdate");
  }

  async domain_get_one() {
    return await this._call_service("/DomainGetDomain");
  }

  //////////////////////////////////////////////////////////
  ///   SOCIAL
  async social_get_all_available() {
    return await this._call_service("/SocialGetAvailable");
  }
  async social_get_domain() {
    return await this._call_service("/SocialGetAll");
  }
  async social_insert() {
    return await this._call_service("/SocialInsert");
  }
  async social_delete() {
    return await this._call_service("/SocialDelete");
  }

  //////////////////////////////////////////////////////////
  ///   CONTACT FORMS

  async contact_forms_get_all_available() {
    return await this._call_service("/FormGetAvailable");
  }
  async contact_forms_get_domain() {
    return await this._call_service("/FormGetAll");
  }
  async contact_forms_insert() {
    return await this._call_service("/FormInsert");
  }
  async contact_forms_delete() {
    return await this._call_service("/FormDelete");
  }

  //////////////////////////////////////////////////////////
  ///   PAYMENTS
  async payments_get_all() {
    return await this._call_service("/PaymentGetAll");
  }
  async payments_get_all_available() {
    return await this._call_service("/PaymentGetAvailable");
  }
  async payments_insert() {
    return await this._call_service("/PaymentInsert");
  }
  async payments_delete() {
    return await this._call_service("/PaymentDelete");
  }

  texts_get_all() {
    return this._call_service("/getDocumentList");
  }

  GO_TO_PREVIOUS_PAGE() {
    this.router.push("/dashboard");
  }

  async HANDLE_PAGE_APP_SERVICE(param) {
    var service_name =
      param.title == "RequestMySitePage"
        ? "my-sites"
        : param.isEdit
        ? "edit-site"
        : "add-site";
    return await this._call_service(`/dashboard/${service_name}`);
  }
}

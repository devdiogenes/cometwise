import BF_APP_COMMON from "./BF_APP_COMMON";
import md5 from "md5";
require("md5");

export default class BFS extends BF_APP_COMMON {
  constructor(router) {
    super(router);
  }

  async login() {
    this.data.token = this._token_calculator(this.data.user, this.data.pass);
    //console.log(this.data);
    var resp = await this._call_service("/Blogin").then(() =>
      localStorage.setItem("token", this.data.token)
    );
    return resp;
  }

  async logout() {
    this._call_service("/Blogout");
    localStorage.clear();
    this.router.push("/login");
  }

  async dummy() {
    var resp = await this._call_service("/Blogin");
  }

  async password_reset() {
    return await this._call_service("/BpasswordReminder");
  }

  async password_save() {
    return await this._call_service("/BPasswordSave");
  }

  async register() {
    return this._call_service("/BuserRegister");
  }

  async getUserProfile() {
    return this._call_service("/PGetDetailsUser");
  }

  async getUserPlans() {
    return this._call_service("/BgetUserPlans");
  }

  async updateUserProfile() {
    return this._call_service("/BuserUpdate");
  }

  async suscribe_plan() {
    return this._call_service("/BsuscribePlan");
  }

  async resetPassword() {
    return this._call_service("/forgotPassword");
  }

  async landingContact() {
    return this._call_service("/formLanding");
  }

  async getPlansAll() {
    return this._call_service("/BgetPlanList");
  }

  check_timeout() {
    return this.axios_int.check_timeout();
  }

  _token_calculator(email, pass) {
    return md5(email + pass);
  }
}
